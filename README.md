# jira-software

Jira Software

Software versions:
- Jira 8.5.7
- Jira Core 8.5.7
- Java 1.8.0_222
- Mysql connector: 5.1.41
- Mysql 5.7

# Build and run docker image
- Reference code: https://github.com/haxqer/jira

- Build docker image
```
docker build -t edtroleis/jira:1.0 .
```

- Run without docker-compose
```
docker run -p 8080:8080 -v jira_home_data:/var/jira --network jira-network --name jira-server -e TZ='America/Sao_Paulo' edtroleis/jira1:0
```

# Use to troubleshooting
```
docker run -d -p 8080:8080 -v jira_home_data:/var/jira --name jira-server edtroleis/jira:1.0 sleep 10000

docker exec -it java-server bash

docker stats
```

# List volumes and prune them
```
docker volume ls
```

# Run Jira with Mysql using docker-compose
```
docker-compose up -d
docker-compose logs
docker-compose ps
docker-compose down

Database Type: MySQL 5.7+
Hostname: mysql-jira
Port: 3306
Database: jira
Username: jira
Password: 123123
```

# Access Jira Software
http://localhost:8080
